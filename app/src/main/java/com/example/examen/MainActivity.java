package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Recibo recibo;
    private EditText txtNombre;
    private Button btnIr;
    private Button btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNombre = (EditText)findViewById(R.id.txtNombre);
        btnIr = (Button) findViewById(R.id.btnIr);
        btnSalir = (Button) findViewById(R.id.btnSalir);

        recibo = new Recibo();

        btnIr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cliente = txtNombre.getText().toString();

                if (cliente.matches("")){
                    Toast.makeText(MainActivity.this, "Falto capturar Nombre",Toast.LENGTH_SHORT).show();
                }
                else {
                    Intent intent = new Intent(MainActivity.this,ReciboNominaActivity.class);

                    // Enviar un dato String
                    intent.putExtra("cliente", cliente);

                    //Enviar un objeto

                    Bundle objeto = new Bundle();
                    objeto.putSerializable("cotizacion",recibo);



                    intent.putExtras(objeto);

                    startActivity(intent);
                }
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



    }
    }

