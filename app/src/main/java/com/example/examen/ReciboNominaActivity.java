package com.example.examen;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ReciboNominaActivity extends AppCompatActivity {
    private Recibo recibo;
    private Recibo getcalcularPagoMensual;
    private Recibo getGetcalcularPagoMensual;
    private RadioGroup radGrupo;
    private TextView lblFolio;
    private TextView lblNombre;
    private TextView lblImpuesto;
    private TextView lblSubTotal;
    public Button btnRegresar;
    private Button btnLimpiar;
   private  TextView txtHorasTrab;
   private TextView txtHorasExtras;
    private Button btnCalcular;
    private RadioButton rad1;
    private RadioButton rad2;
    private RadioButton rad3;
    private Object Recibo;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo);
        lblFolio = (TextView) findViewById(R.id.lblFolio);
        lblNombre = (TextView) findViewById(R.id.lblNombre);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        txtHorasTrab = (TextView) findViewById(R.id.txtHorasTrab);
        txtHorasExtras= (TextView) findViewById(R.id.txtHorasExtras);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        rad1 = (RadioButton) findViewById(R.id.rad1);
        rad2 = (RadioButton) findViewById(R.id.rad2);
        rad3 = (RadioButton) findViewById(R.id.rad3);
        radGrupo = (RadioGroup) findViewById(R.id.radGrupo) ;

        Bundle datos = getIntent().getExtras();
        lblNombre.setText("Nombre: " + datos.getString("cliente"));

        Recibo = (Recibo) datos.getSerializable("cotizacion");
       // Recibo

        // calcularSubTotal = (Recibo) datos.getSerializable("calcularPagoMensual");



        //lblFolio.setText("Folio: " + String.valueOf(Recibo.getFolio()));

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Recibo.setHorasTrabNormal(Float.valueOf(txtHorasTrab.getText().toString()));
               // Recibo.setHorasExtras(Float.valueOf(txtHorasExtras.getText().toString()));
                if (rad1.isChecked())
                {
                    recibo.setPuesto(50);
                }else {
                    if (rad2.isChecked()) {
                        recibo.setPuesto(70);
                    } else {
                        if (rad3.isChecked()) {
                            recibo.setPuesto(100);

                        }
                    }
                }
                //lblSubTotal.setText("El pago mensual es: " + String.valueOf(recibo.getcalcularSubTotal()));
                //lblImpuesto.setText("El enganche es: " + String.valueOf(recibo.getcalcularImpuesto()));
            }



        });


        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtHorasTrab.setText(null);
                txtHorasExtras.setText(null);
                lblImpuesto.setText(null);
                lblSubTotal.setText(null);
                radGrupo.check(R.id.rad1 );

            }
        });


    }




}

