package com.example.examen;
import java.io.Serializable;
import java.util.Random;

public class Recibo implements Serializable {
    private int folio;
    private String nombre;
    private Float pagoHorasTrab;
    private Float pagoHorasExtra;
    private Float horasTrabNormal;
    private Float horasTrabExtras;
    private int puesto;
    private float impuestos;
    private float subtotal;

    public Recibo(int folio,float pagoHorasTrab,float pagoHorasExtra, float horasTrabNormal, float horasTrabExtras, int puesto) {
        this.folio = this.generarFolio();
        this.pagoHorasTrab = pagoHorasTrab;
        this.pagoHorasExtra = pagoHorasExtra;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtras = horasTrabExtras;
        this.puesto = puesto;
    }

    public Float getPagoHorasTrab() {
        return pagoHorasTrab;
    }

    public void setPagoHorasTrab(Float pagoHorasTrab) {
        this.pagoHorasTrab = pagoHorasTrab;
    }

    public Float getPagoHorasExtra() {
        return pagoHorasExtra;
    }

    public void setPagoHorasExtra(Float pagoHorasExtra) {
        this.pagoHorasExtra = pagoHorasExtra;
    }

    public Recibo() {
        this.folio = this.generarFolio();
    }
    public int getFolio() {
        return folio;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Float getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public void setHorasTrabNormal(Float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public Float getHorasTrabExtras() {
        return horasTrabExtras;
    }

    public void setHorasTrabExtras(Float horasTrabExtras) {
        this.horasTrabExtras = horasTrabExtras;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public float getImpuestos() {
        return impuestos;
    }

    public void setImpuestos(float impuestos) {
        this.impuestos = impuestos;
    }


   // public float getcalcularEnganche(){ return calcularEnganche();}

  //  public float getcalcularPagoMensual(){ return calcularPagoMensual();}


    //Comportamiento

    public int generarFolio() {
        return new Random().nextInt()%1000;
    }

    public float calcularSubTotal(float subtotal){
        return this.subtotal =horasTrabNormal* pagoHorasTrab + (pagoHorasExtra* horasTrabExtras);
    }

    public float impuestos(){
        float impuesto = subtotal* .16 ;
        return impuestos();
    }

   // public float calcularPagoMensual(){
        //float enganche = this.calcularEnganche();
        //float totalfin = this.valorAuto - enganche;

        // return totalfin / this.puesto;
    //}
}

